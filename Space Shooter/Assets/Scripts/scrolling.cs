﻿using UnityEngine;
using System.Collections;

public class scrolling : MonoBehaviour {
    //the point of this script is to allow us to make edits to one material without making edits to all the other related materials at once
	void Update()
    {
        MeshRenderer mr = GetComponent<MeshRenderer>();

        Material mat = mr.material; //grabs first instance of material
        Vector2 offset = mat.mainTextureOffset;
        offset.y += Time.deltaTime / 14f;

        mat.mainTextureOffset = offset;
    }
}
