﻿using UnityEngine;
using System.Collections;

public class bolt_mover : MonoBehaviour {
    Rigidbody basicBolt;
    public float speed;
	// Use this for initialization
	void Start () {
        basicBolt = GetComponent<Rigidbody>();
        basicBolt.velocity = transform.forward * speed;
	}
}
